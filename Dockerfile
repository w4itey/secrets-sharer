FROM python:3.8-slim

LABEL maintainer="ken.schiano@gmail.com"

RUN mkdir /app
WORKDIR /app
	
ADD Requirements.txt /app


RUN pip3 install -r Requirements.txt

ADD . /app
EXPOSE 5000

ENTRYPOINT [ "gunicorn", "--config", "gunicorn_config.py", "wsgi:app" ]
