from flask import Flask, render_template, request
from cryptography.fernet import Fernet
import random
import string
import os
import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from dateutil import parser
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/data.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class secrets(db.Model):

    label = db.Column(db.String, primary_key=True)
    msg = db.Column(db.String, nullable=False)
    date = db.Column(
        db.String, nullable=False, default=datetime.datetime.now()
        )
    instant = db.Column(db.String, nullable=False, default=True)
    keep = db.Column(db.String, nullable=False, default='1h')


class Stale:

    def __init__(self):

        self.currentTime = datetime.datetime.now()

    def onehour(self):

        data = secrets.query.filter_by(keep='1h').all()

        for item in data:
            date = parser.parse(item.date)

            if date > date - datetime.timedelta(hours=1):
                secrets.query.filter_by(label=item.label).delete()
                db.session.commit()

    def days(self, search, day):

        data = secrets.query.filter_by(keep=search).all()

        for item in data:
            date = parser.parse(item.date)
            if date > date - datetime.timedelta(day):
                secrets.query.filter_by(label=item.label).delete()
                db.session.commit()

    def purge(self):

        data = secrets.query.filter(
            secrets.date > self.currentTime - datetime.timedelta(30)
            )

        for item in data:

            secrets.query.filter_by(label=item.label).delete()
            db.session.commit()

    def run(self):

        self.onehour()
        self.days('1d', 1)
        self.days('3d', 3)
        self.days('7d', 7)
        self.purge()


def onehourrun():

    x = Stale()
    x.run()


sched = BackgroundScheduler(daemon=True)
sched.add_job(onehourrun, 'interval', minutes=5)
sched.start()


@app.route('/')
def index():

    return render_template('home.html')


@app.route('/encrypt', methods=['POST', 'GET'])
def encrypt():
    try:

        if os.path.exists('data.db') is not True:
            db.create_all()

        host = request.url_root
        host = host.replace('http', 'https')
        deletestring = {
            '1h': 'one (1) hour',
            '1d': 'one (1) day',
            '3d': 'three (3) days',
            '7d': 'seven (7) days'
        }
        text = request.form['EncryptMe'].encode()
        delete = request.form.get('Delete')
        keep = request.form['Time']

        if delete:
            selfdestruct = 'single-use'
        else:
            selfdestruct = ""

        deletetime = deletestring[keep]
        key = Fernet.generate_key()
        f = Fernet(key)
        encrypted_message = f.encrypt(text)
        letters = string.ascii_lowercase
        label = ''.join(random.choice(letters) for i in range(25))

        data = secrets(
            label=label, msg=encrypted_message, instant=delete, keep=keep,
            )

        db.session.add(data)
        db.session.commit()

        return render_template(
            'encrypt.html',
            url=f'{host}decrypt?label={label}&key={key.decode()}',
            short=f'{host}decrypt?label={label}',
            label=label,
            key=key.decode(),
            selfdestruct=selfdestruct,
            deletetime=deletetime
            )

    except Exception:
        return render_template('error.html')


@app.route('/decrypt', methods=['POST', 'GET'])
def decrypt():

    try:

        host = request.url_root

        if request.method == "POST":
            label = request.form['label']
            key = request.form['key']

        if request.method == "GET":
            label = request.args.get('label')
            key = request.args.get('key')

        if label is None:
            return render_template('decrypt.html', label=label, key=key)

        try:
            data = secrets.query.filter_by(label=label).first()
            msg = data.msg
            f = Fernet(key)
            decrypted_message = f.decrypt(msg)
            decrypted_message = decrypted_message.decode()

            if data.instant == 'Delete':
                secrets.query.filter_by(label=label).delete()
                db.session.commit()

        except Exception:

            return render_template('recovered.html')

        return render_template(
            'decrypt.html',
            label=label,
            key=key,
            msg=decrypted_message,
            host=host
            )

    except Exception:
        return render_template('error.html')


if __name__ == '__main__':

    app.run(host='0.0.0.0')
